#Version 1.2

DANIEL_M2 = 12.0
LEO_M2 = 12.7
MARTIN_M2 = 17.0
APARTMENT_M2 = 110.0
RENT = 2250.
UTILITIES = 230.
INTERNET  = 108.0/2 # 1118.00 / 2#129.40 / 2.

PARKING = 80.

class Person():
    def __init__(self, name):
        self.name = name 
    def __repr__(self):
        return self.name

class Cost():
    def __init__(self, payers, total_cost, tag = 'Cost'):
        if hasattr(payers,     '__iter__'):
            self.payers = payers
        else:
            self.payers =  [ payers]
        self.total_cost = total_cost
        self.tag = tag 
    def get_payers(self):
        return self.payers
    
    
class Room():
    def __init__(self, inhabitants, surface, tag = 'room'):
        if hasattr(inhabitants,     '__iter__'):
            self. inhabitants = inhabitants
        else:
            self.inhabitants =  [ inhabitants ]
        self.surface = surface
        self.tag = tag 
    def get_inhabitants(self):
        return self.inhabitants
    

class Apartment():
    def __init__(self, rooms, rent, costs, surface):
        self.rooms = rooms
        self.inhabitants  = []
        self.inhabitants = list(set([inhabitant for room in self.rooms for inhabitant in room.get_inhabitants() ]))
        self.rent = rent 
        self.costs = costs
        self.surface = surface
    def get_inhabitants(self):
        return self.inhabitants
    def split_rent(self):
        rent_dict = {person.name : [] for person in self.inhabitants}
        for room in self.rooms:
            nr_of_inhabitants = len(room.get_inhabitants())
            for inhabitant in room.get_inhabitants():
                rent_dict[inhabitant.name].append([ self.rent * room.surface / apartment.surface / nr_of_inhabitants, room.tag]) 
        return rent_dict
    def split_costs(self):
        cost_dict = {person.name : [] for person in self.inhabitants}
        for cost in self.costs:
            nr_of_payers = len(cost.get_payers())
            for payer in cost.get_payers():
                cost_dict[payer.name].append([ cost.total_cost / nr_of_payers, cost.tag] ) 
        return cost_dict
    def split_all(self):
        rent_dict = self.split_rent()
        additional_cost_dict = self.split_costs()
        alltogether = 0.
        for name,rent_list in rent_dict.items():
            print name.upper()
            print '   Rent for your rooms:'
            for rent, tag in rent_list:
                print '      {:.2f} ({})'.format(rent, tag)
            print '   Additional costs:'
            cost_list = additional_cost_dict[name]
            for cost, tag in cost_list:
                print '      {:.2f} ({})'.format(cost, tag)
            total = sum(zip(*rent_list)[0]) + sum(zip(*cost_list)[0])
            print  '   Total: \n      %.2f'%total
            alltogether += total 
        print 'Total sum: %.2f'%alltogether
        
        
        

martin  =  Person('Martin')
# irene = Person('Irene')
leo = Person('Leo')
daniel =  Person ('Daniel')

# ~ room_MI = Room([martin, irene] , MARTIN_M2, 'bedroom MI')
room_MI = Room(martin , MARTIN_M2, 'bedroom MI')
room_leo = Room(leo, LEO_M2, 'bedroom LEO')

# ~ parking = Cost([martin, irene], PARKING, 'parking')
    
room_daniel    = Room(daniel, DANIEL_M2, 'bedroom Daniel')
# ~ common_space = Room([martin, irene, leo, daniel], APARTMENT_M2 - LEO_M2 - MARTIN_M2 - DANIEL_M2, 'common space')
# ~ utilities    = Cost([martin, irene, leo, daniel], UTILITIES, 'utilities')
# ~ internet     = Cost([martin, irene, leo, daniel], INTERNET, 'internet')
common_space = Room([martin, leo, daniel], APARTMENT_M2 - LEO_M2 - MARTIN_M2 - DANIEL_M2, 'common space')
utilities    = Cost([martin, leo, daniel], UTILITIES, 'utilities')
internet     = Cost([martin, leo, daniel], INTERNET, 'internet')
apartment    = Apartment([room_MI, room_leo, room_daniel, common_space], RENT, [utilities, internet], APARTMENT_M2)
# ~ apartment    = Apartment([room_MI, room_leo, room_daniel, common_space], RENT, [utilities, internet, parking], APARTMENT_M2)

apartment.split_all()
